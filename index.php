<?php

require './config.php';

	function cargarClasesAutomaticamente($class){

		if( file_exists(_LIBS.$class.".php") ){
			require _LIBS.$class.".php";
		}

	}

	spl_autoload_register('cargarClasesAutomaticamente');

	/**
		Ejemplo
	*	pokedex.com/pikachu/nombre
	*
	*	1. Es una carpeta lo que él busca? existe? -> no
	*	2. Es un archivo lo que él busca? existe? -> no
	*	3. es una librería lo que él busca? existe? -> no
	*
	*	Entonces?
	*	Mmmm vea, siga derecho y pregunte en esa puerta del apartamento index si allí
	*	está la persona que usted busca.
	*
	*/
	// var = (condición) ? verdero : falso;
	$url = (isset($_GET["url"])) ? $_GET["url"] : "index";
	$url = explode("/", $url); // [0] => pikachu, [1] => lo que quiero
	
	if( file_exists( "./controllers/".$url[0].".php") ){
		require "./controllers/".$url[0].".php";
	}else{
		require './views/404.php';
	}