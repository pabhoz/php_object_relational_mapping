<?php

class Region extends ORM{

	protected static $table ="region";
	public $id,$region;

	function __construct($id,$region){

		$this->id = $id;
		$this->region = $region;

	}
	//Escribir mètodo en ORM y sobreescribirlo en las clases
	function get( $id ){
		$data = array_shift(self::where("id",$id));
		$region = new self($data["id"],$data["region"]);
		return $region;
	}

	function getByName( $region ){
		$data = array_shift(self::where("region",$region));
		//Logger::debug("data",$data,"getByName");
		$region = new self($data["id"],$data["region"]);
		return $region;
	}


}