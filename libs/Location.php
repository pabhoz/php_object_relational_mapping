<?php

class Location extends ORM{

	protected static $table = "location";
	public $id,$location,$Region_id;

    public $has_many = array();

    public $has_one = array(
    	'region'=>array(
    			'class'=>'Region',
	            'join_as'=>'Region_id',
	            'join_with'=>'id',
	            'fkey_table'=>'region'
    		)
    	);
                
	function __construct($id,$location,$region_id){

		$this->id = $id;
		$this->location = $location;
		$this->Region_id = $region_id;

	}

	function get( $id ){
		$data = array_shift(self::where("id",$id));
		$usr = new self($data["id"],$data["location"],$data["Region_id"]);
		return $usr;
	}

	function getByName( $location ){
		$data = array_shift(self::where("location",$location));
		//Logger::debug("data",$data,"getByName");
		$usr = new self($data["id"],$data["location"],$data["Region_id"]);
		return $usr;
	}

}